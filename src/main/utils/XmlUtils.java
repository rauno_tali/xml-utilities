package main.utils;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;

// Rauno did this.
// This class is designed to read an xml file to return TestNG-s testDataArray

public class XmlUtils {
	
	// logger
	
	private static final Logger log = LogManager.getLogger(XmlUtils.class);
	
	// testdata.xml validation helper functions
	
	private static void validateCurrentDataSetSize(int currentDataSetSize, int firstDataSetSize, String testName){
		if(currentDataSetSize != firstDataSetSize){
			log.error("Malformed xml file: data sets for test \"" + testName + "\" vary in size. "
					+ "First data set is " + firstDataSetSize + " items, found data set with " + currentDataSetSize + " items");
			System.exit(3);
		}
		if(currentDataSetSize == 0){
			log.error("Malformed xml file: a data set for test \"" + testName + "\" is empty.");
			System.exit(3);
		}
	}
	
	private static void validateDataSetsExist(List<Element> dataSets, String testName){
		if (dataSets.isEmpty()){
			log.error("Malformed xml file: no data sets for test \"" + testName + "\".");
			System.exit(3);
		}
	}
	
	private static void validateTestElementExists(Element element, String testName){
		if (element == null) {
			log.error("Malformed xml file: test element \"" + testName + "\" not found within root element.");
			System.exit(3);
		}
	}
	
	// helper functions
	
	private static Element getRootElement(String path){
		Document document = null;
		try {
			File inputFile = new File(path);
			SAXBuilder saxBuilder = new SAXBuilder();
			document = saxBuilder.build(inputFile);
		}
		catch (Exception e)	{
			log.error("Unable to access xml file!\n" + e.getMessage());
			System.exit(1);
		}
		return document.getRootElement();
	}
	
	private static List<Element> getTestDataSets(String path, String testName){
		Element rootElement = getRootElement(path);
		
		Element currentTest = rootElement.getChild(testName);
		validateTestElementExists(currentTest, testName);
		
		List<Element> testDataSets = currentTest.getChildren();
		validateDataSetsExist(testDataSets, testName);
		
		return testDataSets;
	}
	
	// public functions
	
	public static String[][] getTestDataArray(String path, String testName) {
		
		String[][] testDataArray = null;
		
		List<Element> testDataSets = getTestDataSets(path, testName);
		
		int dataSetIterationIndex = 0;
		for (Element currentDataSet : testDataSets){
			List<Element> currentDataSetList = currentDataSet.getChildren();
			
			if (dataSetIterationIndex == 0) testDataArray = new String[testDataSets.size()][currentDataSetList.size()];
			
			validateCurrentDataSetSize(currentDataSetList.size(), testDataArray[0].length, testName);
			
			int dataItemIterationIndex = 0;
			for (Element currentDataItem : currentDataSetList){
				testDataArray[dataSetIterationIndex][dataItemIterationIndex] = currentDataItem.getText();
				dataItemIterationIndex++;
			}
			
			dataSetIterationIndex++;
		}
        
        return testDataArray;
	}

}
