package main.utils;

import java.nio.file.FileSystems;
import java.nio.file.Path;

public class Constants {
	
	public static final Path REPORTS_PATH = FileSystems.getDefault().getPath("reports");
	
	public static final String REPORT_FILE_NAME = "report.html";
	
	public static final String SCREENSHOT_FOLDER_NAME = "screenshots";
	
	public static final Path TESTDATA_PATH = FileSystems.getDefault().getPath("testdata");
	
	public static final String TESTDATA_FILE = "testdata.xml";

}
