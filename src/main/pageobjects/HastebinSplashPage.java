package main.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HastebinSplashPage {
	
	//dirty quick absolute xpath for test
	@FindBy (xpath="/html/body/textarea")
	private WebElement textArea;
	
	public void write(String stuff){
		this.textArea.sendKeys(stuff);
	}
}
