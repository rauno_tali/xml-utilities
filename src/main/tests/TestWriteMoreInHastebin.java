package main.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestWriteMoreInHastebin extends TestBase{

	@Test(dataProvider = "TestData")
	public void test(String firstDataItem, String secondDataItem, String thirdDataItem)	{
		driver.get("http://hastebin.com");
		
		StringBuilder text = new StringBuilder();
		text.append("I am another automated test. I am trying to read three data items from a testdata file. Here's what I found:" + "\n");
		text.append(firstDataItem + "\n");
		text.append(secondDataItem + "\n");
		text.append(thirdDataItem);
		
		hastebinSplashPage.write(text.toString());
		
		Assert.assertTrue(false, "This assert set to always fail to save screenshot.");
	}
}
