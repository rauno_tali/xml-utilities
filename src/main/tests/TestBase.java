package main.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import main.pageobjects.HastebinSplashPage;
import main.utils.Constants;
import main.utils.CustomLogger;
import main.utils.XmlUtils;

public class TestBase implements ITest{
	
	public static void logInfo(String logMes)	{
		CustomLogger.logInfo(logMes);
	}
	
	public static void logError(String logMes)	{
		CustomLogger.logError(logMes);
	}
	
	
	protected static WebDriver driver;
	
	protected static HastebinSplashPage hastebinSplashPage;
	
	
	protected static String testName = "Test Name Initialized";
	
	public String getThisTestName(){
		return this.getClass()
			.getSimpleName()
			.replace("Test", "")
			.replaceAll("(\\p{Ll})(\\p{Lu})","$1 $2");
	}
	
	@Override
	public String getTestName() {
		return testName;
	}

	@BeforeSuite
	public void setUpSuite() {
		CustomLogger.setUpExtentReport();
	}
	
	@BeforeMethod
	public void setUpMethod() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		testName = getThisTestName().concat(" ");
		
		hastebinSplashPage = PageFactory.initElements(driver, HastebinSplashPage.class	);
		
	}
	
	@DataProvider(name = "TestData")
	public Object[][] TestData() throws Exception{
        Object[][] testObjArray = XmlUtils.getTestDataArray(Constants.TESTDATA_PATH + "\\" + Constants.TESTDATA_FILE, getThisTestName().replace(" ", "_").toLowerCase());
        return (testObjArray);
	}
	
	@AfterMethod
	public void teadDownMethod() {
		driver.close();
		driver.quit();
		TestBase.testName = "Test name reset by @AfterMethod";
	}

	@AfterSuite
	public void tearDownSuite()	{
		CustomLogger.finishExtentReport();
	}
	
	public static WebDriver getDriver()	{
		return driver;
	}

}
